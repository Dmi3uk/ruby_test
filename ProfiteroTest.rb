require 'nokogiri'
require 'curb'
require 'csv'
require 'logger'

logger = Logger.new File.new('profitero.log', 'w')
logger.info("Start program / сreated Logger")

urlPages = [] #массив для url всех страниц с товарами
PAGESTART = ARGV[0]
urlPages.push PAGESTART
urlStart = Curl::Easy.perform(PAGESTART)
html = Nokogiri::HTML(urlStart.body_str)

pageIndex = 1
begin
    pageIndex +=1
    pageNext = ARGV[0] + "?p=" + pageIndex.to_s
    urlNext = Curl::Easy.perform(pageNext)
    html = Nokogiri::HTML(urlNext.body_str)
    urlPages.push (pageNext)
    button = html.at('//button[contains(@class, "loadMore")]')
end while !(button.class == NilClass)
logger.debug("All pages with goods are received")

urlProducts = [] #массив для url всех товаров
easy_options = {:follow_location => true}
multi_options = {:pipeline => Curl::CURLPIPE_HTTP1}
Curl::Multi.get(urlPages, easy_options, multi_options) do |easy|
  htmlProduct = Nokogiri::HTML(easy.body_str)
    urlProductsByPage = htmlProduct.xpath('//*[@id="product_list"]//div[1]/div/div[1]/a/@href').each do |row|
            urlProducts.push (row.text)
    end
end
logger.debug("All product pages received")

products = [] #массив для товаров
Curl::Multi.get(urlProducts, easy_options, multi_options) do |easy|
    htmlProduct = Nokogiri::HTML(easy.body_str)
    name = htmlProduct.xpath('//*[@id="center_column"]/div/div/div[2]/div[2]/h1').text.strip
    weight = []
    htmlProduct.xpath('//*[@id="attributes"]/fieldset/div/ul//label/span[1]/text()').each do |i|
        weight.push (i.text)
    end
    price = []
    htmlProduct.xpath('//*[@id="attributes"]/fieldset/div/ul//label/span[2]/text()').each do |j|
        price.push (j.text.chomp("€/u"))
    end
    img = htmlProduct.xpath('//*[@id="bigpic"]/@src').text.strip

    (0..weight.length-1).each do |k|
        products.push(
        name: name + "-" + weight[k],
        price: price[k],
        img: img
        )
    end
end
logger.debug("All products are received")

CSV.open('my.csv', 'w') do |csv|
    products.each do |prod|
    csv << prod.values
  end
end

logger.info("Program finished. The results are written to a file 'my.csv'. ")
